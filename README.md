# Where's my Nobel Prize?

The stories of some under-represented and under-appreciated scientists in Particle Physics.

*Abstract*: Modern day particle physics is a global effort, with multiple countries and cultures contributing the advancement of the field. In many ways, this has not changed since the advent of the field in the early 1900s. What has changed is the recognition we give to all the excellent scientists who have done the work. The lives of under-privileged physicists of the past seldom make it into history books. During this two hour course, we will share some of their stories.

Contact: [Yannick Ulrich](mailto:yannick.ulrich@durham.ac.uk)

## Format and material

This course consists of two lectures at 11am in OC218 (James Stirling Room) and on [Zoom](https://durhamuniversity.zoom.us/j/99173205443?pwd=b1d5WTNXS212amRKNEp1SEpHSFhhZz09)

<table>
    <thead>
        <tr>
            <th>Date</th><th>Topic</th><th>Material</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>12 May</td>
            <td>introduction to the topic and two themes (wives and families of scientists & eurocentrism)</td>
            <td><a href="http://yannickulrich.gitlab.io/where-is-my-nobel-prize/slides.pdf">slides</a>, <a href="https://durham.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=0cfdda4a-76ad-4f10-acd8-b00000b56105">recording</a></td>
        </tr>
        <tr>
            <td>15 May</td>
            <td>discussion of more examples, based on participant input</td>
            <td></td>
        </tr>
    </tbody>
</table>

This course was organised by the IPPP EDI Group.
The lecturers are
[Ansh Bhatnagar](mailto:ansh.bhatnagar@durham.ac.uk),
[Sofie Erner](mailto:sofie.n.erner@durham.ac.uk),
[James Maxwell](mailto:james.l.maxwell@durham.ac.uk),
[Tommy Smith](mailto:tommy.smith@durham.ac.uk),
[Tom Stone](mailto:thomas.w.stone@durham.ac.uk),
[Yannick Ulrich](mailto:yannick.ulrich@durham.ac.uk).

[![IPPP EDI Group](https://img.shields.io/twitter/follow/IPPP_EDI_Durham?style=social)](https://twitter.com/IPPP_EDI_Durham)

## Further reading

### Suggestions for people to look into
*Note*: this is a random collection of people we stumbled across when researching this course.
It is by no means complete.
Please get [in touch](mailto:yannick.ulrich@durham.ac.uk) with your suggestions and we will add them!

 * [Lise Meitner](https://en.wikipedia.org/wiki/Lise_Meitner) (discovered nuclear fission)
 * [Ida Noddack, n&#233;e Tacke](https://en.wikipedia.org/wiki/Ida_Noddack) (theorised nuclear fission and co-discovered rhenium)
 * [Chien-Shiung Wu](https://en.wikipedia.org/wiki/Chien-Shiung_Wu) (proved parity is not conserved in the weak interaction)
 * [Henrietta Leavitt](https://en.wikipedia.org/wiki/Henrietta_Swan_Leavitt) (discovered period-luminosity relation for Cepheid variable stars, building the distance ladder)
 * [Marietta Blau](https://en.wikipedia.org/wiki/Marietta_Blau) (developed photographic particle detectors that were later used to discover the pion)
 * [Jocelyn Bell Burnell](https://en.wikipedia.org/wiki/Jocelyn_Bell_Burnell) (discovered the first pulsars)
 * [Vera Rubin](https://en.wikipedia.org/wiki/Vera_Rubin) (studied galactic rotation curves leading to first evidence of Dark Matter)
 * [Cecilia Payne-Gaposchkin](https://en.wikipedia.org/wiki/Cecilia_Payne-Gaposchkin) (discovered during her PhD that stars are made up of hydrogen and helium)
 * [Elizabeth Langdon Williams](https://en.wikipedia.org/wiki/Elizabeth_Langdon_Williams) (discovered Pluto through its gravitational pull)
 * [Srinivasa Ramanujan](https://en.wikipedia.org/wiki/Srinivasa_Ramanujan) ('His insight *[...]* was *[...]* beyond anything I have met with in any European mathematician')
 * [Katherine Johnson](https://en.wikipedia.org/wiki/Katherine_Johnson) (did orbital calculation during the space race, focus of the [book](https://www.goodreads.com/book/show/25953369-hidden-figures) and [movie](https://en.wikipedia.org/wiki/Hidden_Figures) *Hidden Figures*)

### Theme 1

 * [The Forgotten Life of Einstein's First Wife, *Scientific American*](https://blogs.scientificamerican.com/guest-blog/the-forgotten-life-of-einsteins-first-wife/) ([archived](http://web.archive.org/web/20230511192144/https://blogs.scientificamerican.com/guest-blog/the-forgotten-life-of-einsteins-first-wife/))
 * [The Wives, Sisters, and Helpers of Science, *the Lady Science Podcast*](https://www.ladyscience.com/podcast/episode17-wives-sisters-helpers-of-science) ([archived](http://web.archive.org/web/20230511192213/https://www.ladyscience.com/podcast/episode17-wives-sisters-helpers-of-science))
 * [Helen Lewis: Great Wives, *BBC Radio 4*](https://www.bbc.co.uk/programmes/articles/3P0fbs504rR0dWM3wD3vlsk/five-great-wives-from-history-you-need-to-know-about) ([archived](http://web.archive.org/web/20230511192433/https://www.bbc.co.uk/programmes/articles/3P0fbs504rR0dWM3wD3vlsk/five-great-wives-from-history-you-need-to-know-about))
 * [The Comet Sweeper: Caroline Herschel's Astronomical Ambition](https://www.goodreads.com/book/show/1030860.The_Comet_Sweeper), *Clare Brock*
 * Sch&ouml;ne und bittere Tage -- Mileva Einstein-Mari&#263;, *Babara B&uuml;rki* (paper copy in German only, get in touch)
 * [Caroline Herschel](https://en.wikipedia.org/wiki/Caroline_Herschel) and [Messier 110](https://en.wikipedia.org/wiki/Messier_110), *Wikipedia*
 * [Caroline Herschel, *NASA StarChild*](https://starchild.gsfc.nasa.gov/docs/StarChild/whos_who_level2/herschel.html) ([archived](http://web.archive.org/web/20230512092605/https://starchild.gsfc.nasa.gov/docs/StarChild/whos_who_level2/herschel.html))


### Theme 2
 * [Correcting the Eurocentric History of Mathematics: A Series of Diagrams, *Yohan John*](https://yohanjohn.com/oddsandends/correcting-the-eurocentric-history-of-mathematics-a-series-of-diagrams/) ([archived](http://web.archive.org/web/20230511193841/https://yohanjohn.com/oddsandends/correcting-the-eurocentric-history-of-mathematics-a-series-of-diagrams/))
 * [10 things Eastern thinkers discovered centuries before the West, *The Big Think*](https://bigthink.com/surprising-science/10-things-eastern-thinkers-discovered-centuries-before-the-west/) ([archived](http://web.archive.org/web/20230511193852/https://bigthink.com/surprising-science/10-things-eastern-thinkers-discovered-centuries-before-the-west/))
 * [A World History of Mathematics, *Oxford University*](https://www.maths.ox.ac.uk/about-us/history/world-history-mathematics) ([archived](https://web.archive.org/web/20230511193407/https://www.maths.ox.ac.uk/about-us/history/world-history-mathematics))
 * [The Eurocentric History of Science, *Arun Bala*](https://doi.org/10.1057/9780230601215_3)


### Going further still
 * [The Matthew Matilda Effect in Science, *Social Studies of Science*](https://doi.org/10.1177/030631293023002004)
