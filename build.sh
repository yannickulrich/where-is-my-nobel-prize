#!/bin/sh
set -e

rm -rf public/
mkdir public/


cat <<EOF > public/index.html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
            html * {
                font-family: Georgia, sans-serif;
            }
            #content{
                max-width: 35rem;
                margin: auto auto 2rem;
                padding: 1rem 1rem 0px;
            }
            #content a{
                text-decoration: none;
                border-bottom: 1px dotted rgb(0, 75, 107);
                color: rgb(0, 75, 107);
            }
            h1,h2,h3,h4,h5,h6 {
                font-weight: normal;
                margin: 30px 0px 10px 0px;
                padding: 0px;
                text-align: left;
            }

            h1 { font-size: 240%; margin-top: 0px; padding-top: 0px; }
            h2 { font-size: 180%; }

            td:first-child,th:first-child {
                border-left: none;
                width: 4em;
            }
            td, th {
                border-left: solid 1px;
                padding: 4px;
            }
            th {
                border-bottom: solid 1px;
            }
            table {
                border-collapse: collapse;
            }


        </style>
        <title>Where's my Nobel Prize?</title>
    </head>
    <body>
        <div id="content">
EOF

markdown < README.md >> public/index.html
cp slides.pdf public/

cat <<EOF >> public/index.html
        </div>
    </body>
</html>
EOF
